extern crate rand;	// For AI randomness
use rand::Rng;		// For AI randomness
use std::collections::HashSet;
use std::thread;	// For sleeping
use std::io;		// For terminal interactions

struct Board {
    array: [char; 9],
    recent_spot: usize,
    recent_char: char,
}

impl Board {
    pub fn place(&mut self, spot: usize, player: char) -> bool {
        if spot > 8 || self.array[spot] == ' ' {
            self.array[spot] = player;
            self.recent_spot = spot;
            self.recent_char = player;
            return true;
        }
        return false;
    }

    pub fn print(&self) {
        println!("{} | {} | {}", self.array[0], self.array[1], self.array[2]);
        println!("---------");
        println!("{} | {} | {}", self.array[3], self.array[4], self.array[5]);
        println!("---------");
        println!("{} | {} | {}", self.array[6], self.array[7], self.array[8]);
    }
    
    // Return victor char, or 'P' for a tie, if game is over, otherwise return ' '
    pub fn check_winner(&self) -> char
    {
        // Column-checker
        for x in 0..3 {
            if self.array[x] == self.array[x+3] && 
               self.array[x] == self.array[x+6] && 
               self.array[x] != ' ' 
            { return self.array[x]; }
        }

        // Row-checker	
        for x in (0..7).step_by(3) {
            if self.array[x] == self.array[x+1] && 
               self.array[x] == self.array[x+2] && 
               self.array[x] != ' ' 
            { return self.array[x]; }
        }

        // Top-left diagonal-checker
        if self.array[0] == self.array[4] && 
           self.array[0] == self.array[8] && 
           self.array[0] != ' ' 
            { return self.array[0]; }

        // Top-right diagonal-checker
        if self.array[2] == self.array[4] && 
           self.array[2] == self.array[6] && 
           self.array[2] != ' ' 
            { return self.array[2]; }

        return self.is_cat_game();
    }
    
    // Check for a full (tied) board
    pub fn is_cat_game(&self) -> char
    {
        for x in 0..9 {
            if self.array[x] == ' ' {
                return ' ';
            }
        }

        'P'
    }

    pub fn clear(&mut self) {
        for x in 0..9 {
            self.array[x] = ' ';
        }
        self.recent_spot = 9;
        self.recent_char = ' ';
    }
}

trait Bot {
    fn bot_move(&mut self, _board: &mut Board, _turn: i8) { }
}

struct RandomBot {
    play_char: char,
}

impl Bot for RandomBot {
    fn bot_move(&mut self, board: &mut Board, _turn: i8) {
	    let r = || rand::thread_rng().gen_range(0, 9);
        while !(board.place(r(), self.play_char)){}
    }
}

struct FirstBot {
    play_char: char,
}

impl Bot for FirstBot {
    fn bot_move(&mut self, board: &mut Board, _turn: i8) {
	    let mut x = 0;
        while !(board.place(x, self.play_char)){x += 1;}
    }
}

struct HumanBot {
    play_char: char,
}

impl Bot for HumanBot {
    fn bot_move(&mut self, board: &mut Board, turn: i8) {

		// Ask for user input
		loop 
		{
            let mut s: i8 = 0;
			print!("{}[8;1H", 27 as char);
			println!("Where goes {}?", self.play_char);
			let mut input_text = String::new();
			io::stdin()
				.read_line(&mut input_text)
				.expect("failed to read from stdin");

			let trimmed = input_text.trim();
			match trimmed.parse::<i8>() {
				Ok(i) => s = i,
				Err(..) => println!("Please enter a number 0-8"),
			};
            if board.place(s as usize, self.play_char) {
                break;
            }
		}
    }
}

struct LearnBot {
    play_char: char,
    his: HashSet<[(char, usize); 10]>,
    current_game: [(char, usize); 10],
    precedent: [(char, usize); 10],
    precs: Vec<[(char, usize); 10]>, // Keep a list of possible precedents
}

impl LearnBot {
    fn record_move(&mut self, spot: usize, turn: usize, player: char) {
        self.current_game[turn].0 = player;
        self.current_game[turn].1 = spot;
    }

    fn print_history(&self) {
        println!("LearnBot History:");
        println!("------------------");
        for game in &self.his {
            for turn in 0..9 {
                if game[turn].1 == 9 {
                    break;
                } else {
                    println!("Player {} placed at {}", game[turn].0, game[turn].1);
                }
            }
            println!("Winner: {}\n", game[9].0);
        }
    }

    fn record_winner(&mut self, winner: char) {
        self.current_game[9].0 = winner;
        if winner == self.play_char {
            self.his.insert(self.current_game);
        }
        self.current_game = [(' ', 9); 10];
        self.precs.clear();
    }

    fn get_record_count(&self) -> usize {
        self.his.len()
    }

    // Returns winning turn, if the game is a match. Otherwise, 10
    fn get_winning_turn(&self, game: &[(char, usize); 10], turn: i8) -> i8 {
        // Check each turn in that game
        for x in 0..turn as usize {
            // If that turn matches the current turn
            if self.current_game[x].0 == game[x].0 
            && self.current_game[x].1 == game[x].1 
            && game[x].1 != 9 {
                // If that turn is the most recent turn
                if x == (turn as usize - 1) { //&& r() > 0 {
                    // Check the precedence
                    for spot in 5..9 {
                        if game[spot].0 == ' ' {
                            return spot as i8;
                        }
                    }
                }
            } else {   // If a turn did not match
                return 10;
            }
        }
        10
    }
}

impl Bot for LearnBot {
    fn bot_move(&mut self, board: &mut Board, turn: i8) {
	    let r = || rand::thread_rng().gen_range(0, 9);

        // Find best, rather than first precedence
        let mut lowest_win_turn = 9;
        if self.precs.len() == 0 {
            // Check each game in the history
            for game in &self.his {
                let win_turn = self.get_winning_turn(&game, turn);
                if win_turn <= lowest_win_turn {
                    lowest_win_turn = win_turn;
                    self.precedent = *game;
                    self.precs.push(*game);
                }
                /*
                if lowest_win_turn <= 5 {
                    board.place(game[turn as usize].1, self.play_char);
                    return;
                }
                */
            }
        } else if r() != 0 {
            for game in &self.precs {
                let win_turn = self.get_winning_turn(&game, turn);
                if win_turn < lowest_win_turn {
                    lowest_win_turn = win_turn;
                    self.precedent = *game;
                }
                if lowest_win_turn <= 5 {
                    board.place(self.precedent[turn as usize].1, self.play_char);
                    return;
                }
            }
        }

        if lowest_win_turn < 9 {
            // Place with any precedent
            board.place(self.precedent[turn as usize].1, self.play_char);
        } else {
            // Otherwise, fall back to random placement
            while !(board.place(r(), self.play_char)){}
        }
    }
}

fn draw_hist(history: &Vec<(u64, u64)>) {
    for x in history {
        println!("{} - {}", x.0, x.1);
    }

}

fn draw_bars(left: u64, right: u64) {
    let total = left + right;
    let left_percent = (50*left)/(1*total);
    let right_percent = (50*right)/(1*total);
    print!("{}[2J", 27 as char);
    for x in 0..50 {
        println!();
        match left_percent > (50-x) {
            true  => print!("#####"),
            false => print!("     "),
        }
        print!(" ");
        match right_percent > (50-x) {
            true  => print!("#####"),
            false => print!("     "),
        }
    }
    println!("\n#################");
}

fn main() {
    let mut board = Board {array: [' '; 9], 
                           recent_spot: 9, 
                           recent_char: ' '};

    let mut bot_f = FirstBot {play_char: 'X'};
    let mut bot_x = RandomBot {play_char: 'X'};

    let mut bot_l = LearnBot {play_char: 'L', 
                              his: HashSet::new(), 
                              current_game: [(' ', 9); 10], 
                              precedent: [(' ', 9); 10],
                              precs: Vec::new()};

    let mut bot_me = HumanBot {play_char: 'X'};

    let mut x_wins = 0;
    let mut l_wins = 0;
    let mut ties = 0;

    let mut x_recent_wins = 0;
    let mut l_recent_wins = 0;
    let mut recent_ties = 0;

    let mut game = 0;
    let games_to_play = 20000;

    let mut percents: Vec<(u64, u64)> = vec![];

    //loop {
    for y in 0..games_to_play {
        for x in 0..9 {
            match x % 2 {
                1 => bot_l.bot_move(&mut board, x),
                _ => bot_x.bot_move(&mut board, x),
            };
            bot_l.record_move(board.recent_spot, x as usize, board.recent_char);
            if board.check_winner() != ' ' {
                if board.check_winner() == 'X' {
                    x_wins += 1;
                    x_recent_wins += 1;
                } else if board.check_winner() == 'L' {
                    l_wins += 1;
                    l_recent_wins += 1;
                } else {
                    ties += 1;
                    recent_ties += 1;
                }
                bot_l.record_winner(board.check_winner());
                break;
            }
        }

        let total = x_wins + l_wins + ties;
        let recent_total = x_recent_wins + l_recent_wins + recent_ties;

        if total % 1000 == 0 {
            x_recent_wins /= 100;
            l_recent_wins /= 100;
            recent_ties /= 100;
        }

        board.clear();

        // if game % 100 == 0 {
        //     println!();
        //     println!("X WINS: {}", x_wins);
        //     println!("L WINS: {}", l_wins);
        //     //println!("TIES: {}", ties);
        //     //println!("TOTAL: {}", ties + x_wins + l_wins);
        //     println!("PERCENT: {}", ((100*l_wins) as f32) / (total as f32));
        //     println!("REC_PER: {}", ((100*l_recent_wins) as f32) / (recent_total as f32));
        //     println!("RECORDS: {}", bot_l.get_record_count());
        //     println!();
        // }
        game += 1;
        if y % (games_to_play/100) == 5 {
            draw_bars(l_wins, x_wins);
            percents.push((l_wins, x_wins));
        }
    }
    //draw_hist(&percents);
    draw_bars(l_wins, x_wins);
    /*
    // BRING IN THE HUMAN
    print!("\x1B[2J");
    loop {
    for x in 0..9 {
        match x % 2 {
            1 => bot_me.bot_move(&mut board, x),
            _ => bot_l.bot_move(&mut board, x),
        };
        println!();
        board.print();
        println!();
        bot_l.record_move(board.recent_spot, x as usize, board.recent_char);
        if board.check_winner() != ' ' {
            if board.check_winner() == 'X' {
                x_wins += 1;
                x_recent_wins += 1;
            } else if board.check_winner() == 'L' {
                l_wins += 1;
                l_recent_wins += 1;
            } else {
                ties += 1;
                recent_ties += 1;
            }
            bot_l.record_winner(board.check_winner());
            board.clear();
            break;
        }
        if (x_wins + l_wins + ties) % 1000 == 0 {
            x_recent_wins /= 100;
            l_recent_wins /= 100;
            recent_ties /= 100;
        }
    }
    }
    */

    //bot_l.print_history();
}
